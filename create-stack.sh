#!/bin/bash
cd cassandra
set -x
openstack stack create -e lib/env.yaml \
         --parameter "key_name=fgaudet-key;image_id=Ubuntu Server 16.04_Cassandra-3.10_java8;net_id=fred-net;name=madics-cassandra" \
         -t cassandra.yaml \
        Madics-Cassandra-stack
