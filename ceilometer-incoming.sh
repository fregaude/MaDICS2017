#!/bin/bash
# coding=utf-8
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    <http://www.gnu.org/licenses/>.
#
# F-Gaudet 2017
#
# WARNING : Due to resolution issue (remember this script has been written for Audace2017 talk), I don't rely on server UUID but names. 
# This is not a good practice as you can have 2 VM with the exact same name in OpenStack. So I would suggest you to change that before using this script.

help () {
echo "usage: $0 -d day(s) -s server-name" >&2
}

optspec=":hsd-:"
while getopts "$optspec" optchar; do
    case "${optchar}" in
        h)
            help
            exit 1
            ;;
        s)
            NAME="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
            ;;
        d)
            DAYS="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
            ;;
    esac
done

# Check arg number
if [ -z "$NAME" ] || [ -z "$DAYS" ] ; then
    help
    exit 1
fi

TODAY=$(date -j "+%Y-%m-%d")
PAST_DATE=$(date -j -v-${DAYS}d "+%Y-%m-%d")

# Get UUID from (first...) name
UUID=$(openstack server list | grep $NAME | cut -d '|' -f 2)
UUID=${UUID//[\' ()]}

# Awful hack to retrieve resource_id from image UUID
# Challenge is to retrieve the SRV part of this resource id which is by default only available to admin
PORT_ID=$(ceilometer sample-list --meter network.outgoing.bytes | grep $UUID | cut -d "|" -f 2 | uniq)
PORT_ID=${PORT_ID//[\' ()]}

set -x
ceilometer sample-list --meter network.incoming.bytes \
			--query "resource_id=$PORT_ID;timestamp>${PAST_DATE}T00:00:00;timestamp<=${TODAY}T23:59:59" \
            --limit 5000