 #/bin/bash
set -v
FLAVOR=m1.small
IMAGE="Ubuntu Server 16.04 LTS (xenial)"
NET_ID=11cfde5b-b7d7-4102-8008-d20fa6a56564
KEY=fgaudet-key

openstack server create --flavor $FLAVOR --image "$IMAGE" --key $KEY --nic net-id=$NET_ID madics
