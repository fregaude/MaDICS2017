#!/bin/bash
# coding=utf-8
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    <http://www.gnu.org/licenses/>.
#
# F-Gaudet 2017
#
# WARNING : Due to resolution issue (remember this script has been written for Audace2017 talk), I don't rely on server UUID but names. 
# This is not a good practice as you can have 2 VM with the exact same name in OpenStack. So I would suggest you to change that before using this script.

set -x
openstack stack create -e lib/env.yaml \
         --parameter "key_name=fgaudet-key;image_id=Ubuntu Server 16.04_Cassandra-3.10_java8;net_id=fred-net;name=madics-cassandra" \
         -t cassandra.yaml \
        Madics-Cassandra-stack